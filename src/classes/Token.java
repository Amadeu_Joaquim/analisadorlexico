package classes;

public class Token {
    public static final String TK_IDENTIFIER = "Identificador";
    public static final String TK_NUMBER = "Número";
    public static final String TK_OPERATOR = "Operador logico";
    public static final String TK_ESPECIALSYMBOL = "Simbolo especial";
    public static final String TK_ASSING = "Simbolo de atrubuicao";
    public static final String TK_ERROLEXICO="Simbolo desconhecido na linguagem";
    public static final String TK_KEYWORD="Palavra reservada";

    private String type;
    private String text;

    public Token(String type, String text) {
        this.type = type;
        this.text = text;
    }

    public Token() {
    }

    public static String getTkIdentifier() {
        return TK_IDENTIFIER;
    }

    public static String getTkNumber() {
        return TK_NUMBER;
    }

    public static String getTkOperator() {
        return TK_OPERATOR;
    }

    public static String getTkEspecialsymbol() {
        return TK_ESPECIALSYMBOL;
    }

    public static String getTkErrolexico() {
        return TK_ERROLEXICO;
    }

    public static String getTkKeyword() {
        return TK_KEYWORD;
    }

    public static String getTkAssing() {
        return TK_ASSING;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Token{" +
                "type=" + type +
                ", text='" + text + '\'' +
                '}';
    }
}
