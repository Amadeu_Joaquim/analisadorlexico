package classes;

public class ScannerCompiler {

    private char[] content;
    private int estado;
    private int pos;
    public static int qntCaracteres = 0;

    public ScannerCompiler(String conteudoTextArea) {

        try {

            content = conteudoTextArea.toCharArray();
            pos = 0;

        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    private boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    private boolean isChar(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }

    private boolean isOparator(char c) {
        return c == '>' || c == '<' || c == '=' || c == '!';
    }

    private boolean isSpace(char c) {
        return c == ' ' || c == '\t' || c == '\n' || c == '\r';
    }

    private boolean isSpecialSymbol(char c) {
        return   c == '(' || c == ')' || c == '[' || c == ']' || c == ':'
                || c == ';' || c == ',' || c == '+' || c == '-' || c == '*'
                || c == '.';
    }

    private boolean iskeyWord(String lexema) {

        //        verifica os restantes simbolos especiais da linguagem

        boolean condicao = lexema.equalsIgnoreCase("Program") || lexema.equalsIgnoreCase("of")
                || lexema.equalsIgnoreCase("boolean") || lexema.equalsIgnoreCase("read")
                || lexema.equalsIgnoreCase("var") || lexema.equalsIgnoreCase("char")
                || lexema.equalsIgnoreCase("begin") || lexema.equalsIgnoreCase("write")
                || lexema.equalsIgnoreCase("array") || lexema.equalsIgnoreCase("integer")
                || lexema.equalsIgnoreCase("end") || lexema.equalsIgnoreCase("if")
                || lexema.equalsIgnoreCase("function") || lexema.equalsIgnoreCase("or")
                || lexema.equalsIgnoreCase("true") || lexema.equalsIgnoreCase("false")
                || lexema.equalsIgnoreCase("and") || lexema.equalsIgnoreCase("div")
                || lexema.equalsIgnoreCase("procedure")  || lexema.equalsIgnoreCase("do")
                || lexema.equalsIgnoreCase("while")  || lexema.equalsIgnoreCase("else")
                || lexema.equalsIgnoreCase("then")  || lexema.equalsIgnoreCase("not") ;

        if (condicao) {

            return true;
        }
        return false;
    }


    private char nextChar() {
        try {

            return content[pos++];

        } catch (Exception e) {

        }
        return ' ';
    }

    private boolean isEOF() {
//     isEOF====>>>> is end of file
        return pos == content.length;
    }

    public Token nextToken() {

        char currentChar;
        String lexema = "";
        Token token;

        if (isEOF()) {
            return null;
        }
        estado = 0;
        while (true) {

            currentChar = nextChar();

            qntCaracteres++;

            switch (estado) {
                case 0:
                    if (isChar(currentChar)) {
                        lexema += currentChar;
                        estado = 1;

                    } else if (isDigit(currentChar)) {
                        lexema += currentChar;
                        estado = 3;

                    } else if (isSpace(currentChar)) {
                        lexema += currentChar;
                        estado = 0;

                    } else if (isOparator(currentChar)) {
                        lexema += currentChar;
                        estado = 5;

                    } else if (isSpecialSymbol(currentChar)) {
                        lexema += currentChar;
                        estado = 8;
                    } else {
//
                        estado = 1000;
                    }

                    break;

                case 1:

                    if (isChar(currentChar) || isDigit(currentChar)) {
                        lexema += currentChar;
                        estado = 1;

                    } else if (isSpace(currentChar) || isOparator(currentChar) || isSpecialSymbol(currentChar)) {
                        estado = 2;

                    } else {
                        estado = 1000;
                    }
                    break;
                case 2:
                    back();
                    token = new Token();
                    if (iskeyWord(lexema)) {
                        token.setType(Token.TK_ESPECIALSYMBOL);
                    } else {
                        token.setType(Token.TK_IDENTIFIER);
                    }
                    token.setText(lexema);
                    return token;
                case 3:
                    if (isDigit(currentChar)) {
                        estado = 3;
                        lexema += currentChar;

                    } else if (!isChar(currentChar)) {
                        estado = 4;

                    } else {
                        lexema += currentChar;
                        estado = 1000;
                    }
                    break;
                case 4:
                    back();
                    token = new Token();
                    token.setType(Token.TK_NUMBER);
                    token.setText(lexema);
                    return token;
                case 5:
                    if (isOparator(currentChar)) {
                        estado = 7;
                    } else {
                        estado = 6;

                    }
                case 6:
                    back();
                    token = new Token();
                    token.setType(Token.TK_ASSING);
                    token.setText(lexema);
                    return token;
                case 7:
                    back();
                    token = new Token();
                    token.setType(Token.TK_OPERATOR);
                    token.setText(lexema);
                    return token;
                case 8:
                    back();
                    token = new Token();
                    token.setType(Token.TK_ESPECIALSYMBOL);
                    token.setText(lexema);
                    return token;

                case 1000:
                    back();
                    token = new Token();
                    token.setType(Token.TK_ERROLEXICO);
                    token.setText(lexema);
                    return token;


            }
        }

    }

    private void back() {
        pos--;
    }


}
