package sample;

import classes.ScannerCompiler;
import classes.Token;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;

import java.time.LocalDate;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Controller {

    @FXML
    private Button executarButton;


    @FXML
    private Button limparButton;


    @FXML
    private TextArea textAreaPrincipal;

    @FXML
    private TextArea textAreaDeErros;

    @FXML
    private Label qntTokensLabel;

    @FXML
    private Label qntCaracteresLabel;

    @FXML
    private Label qntErrosLabel1;

    @FXML
    private Label tempoDeExecLabel;

    @FXML
    private TableView<Token> tabelaTokens;

    @FXML
    private TableColumn<Token, String> lexemaTable;

    @FXML
    private TableColumn<Token, String> tokenTable;

    ObservableList<Token> observableList = FXCollections.observableArrayList();
    private int qntTokens = 0; // quantidade inicial de tokens


    @FXML
    void executar(ActionEvent event) {

        long startTime = System.currentTimeMillis();
        metodoPrincipal();
        long stopTime = System.currentTimeMillis();
        tempoDeExecLabel.setText(String.valueOf(Math.round(stopTime - startTime)));
        System.out.println(stopTime - startTime);

    }

    public void metodoPrincipal() {
        ScannerCompiler scanner = new ScannerCompiler(textAreaPrincipal.getText());
        Token token = null;

        do {
            token = scanner.nextToken();
            if (token != null) {
                observableList.add(token);
                qntTokens++;
                System.out.println(token);
            }
        } while (token != null);

        preencherTabelaDeTokens(observableList);

        qntTokensLabel.setText(String.valueOf(qntTokens));

        qntCaracteresLabel.setText(String.valueOf(ScannerCompiler.qntCaracteres));

        ScannerCompiler.qntCaracteres = 0;

        System.out.println("=========Parte do obsevable list ============");
        for (Token t : observableList
        ) {
            System.out.println(t);
        }
    }

    public void preencherTabelaDeTokens(ObservableList<Token> observableList) {

        lexemaTable.setCellValueFactory(new PropertyValueFactory<Token, String>("text"));
        tokenTable.setCellValueFactory(new PropertyValueFactory<Token, String>("type"));
        tabelaTokens.setItems(observableList);


    }


    @FXML
    void limparTela(ActionEvent event) {

        textAreaPrincipal.clear();
        tabelaTokens.getItems().clear();
        qntTokensLabel.setText("");
        qntCaracteresLabel.setText("");
        tempoDeExecLabel.setText("");
        ScannerCompiler.qntCaracteres = 0;
    }


}
